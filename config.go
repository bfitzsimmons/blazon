package main

import "github.com/spf13/viper"

// Config holds the configuration settings for the service.
var Config config

type config struct {
	// Project
	Version string

	// Domain
	Port int

	// Postgresql
	PostgresHost           string
	PostgresPort           uint16
	PostgresUser           string
	PostgresPassword       string
	PostgresDatabase       string
	PostgresMaxConnections int

	// Logging
	LoggingLevel string

	// Misc.
	HandlerTimeout int
}

// InitializeConfigs sets the config. settings either from environment variables or from hard coded default values.
func InitializeConfigs() error {
	viper.SetEnvPrefix("blazon")
	viper.AutomaticEnv()
	viper.SetDefault("Version", "0.1.0")
	viper.SetDefault("port", 8097)
	viper.SetDefault("PostgresHost", "localhost")
	viper.SetDefault("PostgresPort", 5432)
	viper.SetDefault("PostgresUser", "blazon")
	viper.SetDefault("PostgresPassword", "blazon")
	viper.SetDefault("PostgresDatabase", "blazon")
	viper.SetDefault("PostgresMaxConnections", 5)
	viper.SetDefault("logginglevel", "warn")
	viper.SetDefault("HandlerTimeout", 90)

	if err := viper.Unmarshal(&Config); err != nil {
		return err
	}

	return nil
}
