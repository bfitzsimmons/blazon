package main

import (
	"net/url"
	"strings"
	"time"
)

/*----------------------------------------------------------------------------
Interfaces
----------------------------------------------------------------------------*/

// CreationDatetimer deals with datetimes.
type CreationDatetimer interface {
	GetCreationDatetime() time.Time
	SetCreationDatetime(time.Time)
}

/*----------------------------------------------------------------------------
Utilities
----------------------------------------------------------------------------*/

// SetCreationDatetime set the CreationDatetime field.
func SetCreationDatetime(obj CreationDatetimer, currentTime time.Time) {
	if obj.GetCreationDatetime().IsZero() {
		obj.SetCreationDatetime(currentTime.UTC())
	}
}

// GetParentPath returns the resource type's path.
func GetParentPath(inputURL string) (string, error) {
	URL, err := url.Parse(inputURL)
	if err != nil {
		return "", err
	}

	// Break the URL in pieces and remove any empty indexes.
	newURLParts := []string{""}
	for _, v := range strings.Split(URL.Path, "/") {
		if v != "" {
			newURLParts = append(newURLParts, v)
		}
	}

	return strings.Join(newURLParts[:len(newURLParts)-1], "/"), nil
}
