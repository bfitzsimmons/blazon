package main

import log "github.com/Sirupsen/logrus"

var obj interface{}

func init() {
	// Dial back the logging so that we don't see Error logs when hitting the http handler with bogus data.
	loggingLevel, err := log.ParseLevel("fatal")
	if err != nil {
		log.Panic(err)
	}
	log.SetLevel(loggingLevel)
}
