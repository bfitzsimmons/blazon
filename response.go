package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"path"
	"strconv"

	"bitbucket.org/bfitzsimmons/gadgets"
	log "github.com/Sirupsen/logrus"
)

// Errors
var (
	ErrInvalidStatusCode = errors.New("Invalid status code")
)

/*----------------------------------------------------------------------------
Responses
----------------------------------------------------------------------------*/

// Meta contains extra info. about the response.
type Meta struct {
	Version string `json:"version"`
}

// Map maps values to the Meta struct.
func (meta *Meta) Map() {
	meta.Version = Config.Version
}

// MetaMapper maps meta data.
type MetaMapper interface {
	Map()
}

// PathFinder deals with the response path.
type PathFinder interface {
	GetPath() string
}

// ResponseBody is the body of the response.
type ResponseBody struct {
	Links  LinksMapper `json:"links"`
	Meta   MetaMapper  `json:"meta"`
	Errors []*APIError `json:"errors,omitempty"`
	Data   interface{} `json:"data,omitempty"`
}

// MapErrors maps the APIErrors into the JSON response structure.
func (rb *ResponseBody) MapErrors(response *Response) error {
	// Return if there are no errors to map.
	if response.Error == nil {
		return nil
	}

	// Set the Errors.
	rb.Errors = []*APIError{response.Error}

	// Make sure we have a valid status code.
	if http.StatusText(response.StatusCode) == "" {
		return ErrInvalidStatusCode
	}

	// Log the error if the response is a 5xx.
	if response.StatusCode >= 500 {
		log.Error(fmt.Sprintf("%d: %s --> %s", response.StatusCode, response.Error.Title, response.Error.Detail))
	}

	return nil
}

// Response contains response info., and is to be added to the response context.
type Response struct {
	Request    *http.Request
	Writer     http.ResponseWriter
	Error      *APIError
	StatusCode int
	// Params     ParamMapper
	Links   LinksMapper
	Meta    MetaMapper
	Object  Objecter
	Content interface{}
}

// NewResponse retuns a pointer to a Response instance.
func NewResponse() *Response {
	return &Response{StatusCode: 200}
}

// AddError adds an APIError to the response.
func (response *Response) AddError(id int, msg string, err error) {
	response.Error = &APIError{id, msg, err.Error()}
}

// IsErrors returns whether or not any errors have been set.
func (response *Response) IsErrors() bool {
	if response.Error != nil {
		return true
	}
	return false
}

// AddContent adds content to the response.
func (response *Response) AddContent(obj interface{}) (err error) {
	response.Content, err = json.Marshal(obj)
	return err
}

// SetHeaders sets the headers for the object's response.
func (response *Response) SetHeaders() error {
	// Set the JSONAPI response header.
	response.Writer.Header().Set("Content-Type", "application/vnd.api+json")

	// Don't allow caching.
	response.Writer.Header().Set("Cache-Control", "no-cache")

	// Write the status code to the response.
	if http.StatusText(response.StatusCode) == "" {
		return ErrInvalidStatusCode
	}
	response.Writer.WriteHeader(response.StatusCode)

	// If this is a new object, add the location header.
	if response.StatusCode == 201 {
		URL := path.Join(response.Request.URL.Path, strconv.Itoa(int(response.Object.GetID())))
		response.Writer.Header().Set("Location", URL)
	}

	return nil
}

// MapLinks maps the links in the content object or type into the response body.
func (response *Response) MapLinks(rb *ResponseBody) {
	response.Links.Map(response)
	rb.Links = response.Links
}

// MapMeta maps the meta info. to the response body.
func (response *Response) MapMeta(rb *ResponseBody) {
	response.Meta = &Meta{}
	response.Meta.Map()
	rb.Meta = response.Meta
}

// Encode encodes the response body as JSON.
func (response *Response) Encode(responseBody *ResponseBody) error {
	return json.NewEncoder(response.Writer).Encode(responseBody)
}

// Send sends the response.
func (response *Response) Send(statusCode int) {
	var err error

	responseBody := ResponseBody{}

	// Set the status code.
	if statusCode != 0 && http.StatusText(statusCode) == "" {
		err = ErrInvalidStatusCode
	} else if statusCode != 0 {
		response.StatusCode = statusCode
	}

	// Map the errors.
	responseBody.MapErrors(response)

	// Map the JSON API links.
	response.MapLinks(&responseBody)

	// Map the Meta API data.
	response.MapMeta(&responseBody)

	// Map the data.
	responseBody.Data = response.Content

	// Set the headers.
	response.SetHeaders()

	// JSON encode the response body.
	err = response.Encode(&responseBody)

	// Handle errors.
	if err != nil {
		gadgets.ServerError(response.Writer, "Unable to send a proper response", err)
	}
}
