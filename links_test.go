package main

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestListLinks_Map(t *testing.T) {
	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w
	response.Object = &Application{}

	// Set up the links.
	links := ListLinks{}

	// Map the links.
	links.Map(response)
}

func TestCreateLinks_Map(t *testing.T) {
	var err error

	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w

	// Set up the links.
	links := CreateLinks{}

	testData := []struct {
		isErrors bool
		object   Objecter
		err      error
	}{
		{false, nil, ErrNoID},
		{false, &Application{}, nil},
		{true, &Application{}, nil},
		{true, nil, nil},
	}

	for _, data := range testData {
		response.Content = data.object

		err = links.Map(response, data.isErrors)

		if !reflect.DeepEqual(err, data.err) {
			t.Errorf("Expected %v, got %v", data.err, err)
		}
	}
}

func TestDetailLinks_Map(t *testing.T) {
	// Set up the links.
	links := DetailLinks{}

	testData := []struct {
		path     string
		isErrors bool
	}{
		{"/api/apps/45", false},
		{"/api/apps/45", true},
	}

	for _, data := range testData {
		// Set up the request.
		req, err := http.NewRequest("GET", data.path, nil)
		if err != nil {
			t.Fatal(err)
		}

		// Set up the handler.
		w := httptest.NewRecorder()

		// Set up the response.
		response := NewResponse()
		response.Request = req
		response.Writer = w

		// Map the links.
		links.Map(response, data.isErrors)
	}
}

func TestUpdateLinks_Map(t *testing.T) {
	// Set up the links.
	links := UpdateLinks{}

	testData := []struct {
		path     string
		isErrors bool
	}{
		{"/api/apps/45", false},
		{"/api/apps/45", true},
	}

	for _, data := range testData {
		// Set up the request.
		req, err := http.NewRequest("GET", data.path, nil)
		if err != nil {
			t.Fatal(err)
		}

		// Set up the handler.
		w := httptest.NewRecorder()

		// Set up the response.
		response := NewResponse()
		response.Request = req
		response.Writer = w

		// Map the links.
		links.Map(response, data.isErrors)
	}
}

func TestDeleteLinks_Map(t *testing.T) {
	// Set up the links.
	links := DeleteLinks{}

	testData := []struct {
		path     string
		isErrors bool
	}{
		{"/api/apps/45", false},
		{"/api/apps/45", true},
	}

	for _, data := range testData {
		// Set up the request.
		req, err := http.NewRequest("GET", data.path, nil)
		if err != nil {
			t.Fatal(err)
		}

		// Set up the handler.
		w := httptest.NewRecorder()

		// Set up the response.
		response := NewResponse()
		response.Request = req
		response.Writer = w

		// Map the links.
		links.Map(response, data.isErrors)
	}
}

func TestPaginationLinks_Map(t *testing.T) {
	// Set up the response.
	response := NewResponse()

	// Set up the links.
	links := PaginationLinks{}

	// Map the links.
	links.Map(response)

	// TODO: test the properties.
}

func TestPaginationLinks_BuildPrevious(t *testing.T) {
	// Set up the links.
	links := PaginationLinks{}

	// Build the previous URL.
	links.BuildPrevious()

	// TODO: test everything else.
}

func TestPaginationLinks_BuildNext(t *testing.T) {
	// Set up the links.
	links := PaginationLinks{}

	// Build the previous URL.
	links.BuildNext()

	// TODO: test everything else.
}
