package main

import (
	"os"
	"reflect"
	"testing"
)

func resetEnvVars() {
	t := &testing.T{}

	os.Unsetenv("BLAZON_VERSION")
	os.Unsetenv("BLAZON_PORT")
	os.Unsetenv("BLAZON_POSTGRESHOST")
	os.Unsetenv("BLAZON_POSTGRESPORT")
	os.Unsetenv("BLAZON_POSTGRESUSER")
	os.Unsetenv("BLAZON_POSTGRESPASSWORD")
	os.Unsetenv("BLAZON_POSTGRESDATABASE")
	os.Unsetenv("BLAZON_POSTGRESMAXCONNECTIONS")
	os.Unsetenv("BLAZON_LOGGINGLEVEL")
	os.Unsetenv("BLAZON_HANDLERTIMEOUT")

	if err := InitializeConfigs(); err != nil {
		t.Fatalf("Unable to marshal config: %v", err)
	}
}

func TestInitializeConfigs(t *testing.T) {
	/*----------------------------------------------------------------------------
	Test the defaults.
	----------------------------------------------------------------------------*/
	resetEnvVars()

	defaultConfig := config{
		Version:                "0.1.0",
		Port:                   8097,
		PostgresHost:           "localhost",
		PostgresPort:           5432,
		PostgresUser:           "blazon",
		PostgresPassword:       "blazon",
		PostgresDatabase:       "blazon",
		PostgresMaxConnections: 5,
		LoggingLevel:           "warn",
		HandlerTimeout:         90,
	}

	if !reflect.DeepEqual(Config, defaultConfig) {
		t.Errorf("Expected %+v, got %+v", defaultConfig, Config)
	}

	/*----------------------------------------------------------------------------
	Test custom config.
	----------------------------------------------------------------------------*/
	// With proper values.
	resetEnvVars()

	customConfig := config{
		Version:                "0.1.1",
		Port:                   8000,
		PostgresHost:           "test.com",
		PostgresPort:           5432,
		PostgresUser:           "test",
		PostgresPassword:       "test",
		PostgresDatabase:       "test",
		PostgresMaxConnections: 10,
		LoggingLevel:           "warn",
		HandlerTimeout:         60,
	}

	os.Setenv("BLAZON_VERSION", "0.1.1")
	os.Setenv("BLAZON_PORT", "8000")
	os.Setenv("BLAZON_POSTGRESHOST", "test.com")
	os.Setenv("BLAZON_POSTGRESPORT", "5432")
	os.Setenv("BLAZON_POSTGRESUSER", "test")
	os.Setenv("BLAZON_POSTGRESPASSWORD", "test")
	os.Setenv("BLAZON_POSTGRESDATABASE", "test")
	os.Setenv("BLAZON_POSTGRESMAXCONNECTIONS", "10")
	os.Setenv("BLAZON_LOGGINGLEVEL", "warn")
	os.Setenv("BLAZON_HANDLERTIMEOUT", "60")

	if err := InitializeConfigs(); err != nil {
		t.Fatalf("Unable to marshal config: %v", err)
	}

	if !reflect.DeepEqual(Config, customConfig) {
		t.Errorf("Expected %+v, got %+v", customConfig, Config)
	}

	// With improper values.
	resetEnvVars()
	os.Setenv("BLAZON_PORT", "test")
	os.Setenv("BLAZON_HANDLERTIMEOUT", "test")

	err := InitializeConfigs()
	if err == nil {
		t.Errorf("Expected an error, got %+v", Config)
	}
}
