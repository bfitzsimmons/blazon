package main

// Detail is the data portion of the JSON API response body.
type Detail struct {
	Type       string      `json:"type"`
	ID         string      `json:"id"`
	Attributes interface{} `json:"attributes"`
}
