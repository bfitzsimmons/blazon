package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"goji.io"
)

func TestRouter(t *testing.T) {
	r := Router()
	assert.IsType(t, &goji.Mux{}, r)
}
