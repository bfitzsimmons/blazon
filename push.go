package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"golang.org/x/net/context"

	"github.com/RobotsAndPencils/buford/payload"
	"github.com/RobotsAndPencils/buford/payload/badge"
	"github.com/RobotsAndPencils/buford/push"
)

/*----------------------------------------------------------------------------
Requests
----------------------------------------------------------------------------*/

// PushPOSTRequest is the request body for the push API request.
type PushPOSTRequest struct {
	Data struct {
		Attributes struct {
			ApplicationID int32  `json:"application_id"`
			DeviceToken   string `json:"device_token"`
			Message       string `json:"message"`
			Badge         uint   `json:"badge"`
		} `json:"attributes"`
	} `json:"data"`
}

/*----------------------------------------------------------------------------
Responses
----------------------------------------------------------------------------*/

// // PushResponseData is the data portion of the JSON API response body.
// type PushResponseData struct {
// 	Type       string     `json:"type"`
// 	ID         string     `json:"id"`
// 	Attributes Attributer `json:"attributes"`
// }
//
// // PushResponseBody is the JSON stucture used as the body the object creation response.
// type PushResponseBody struct {
// 	Links struct {
// 		Self *Self `json:"self"`
// 	} `json:"links"`
//
// 	Errors []*APIError        `json:"errors,omitempty"`
// 	Data   []PushResponseData `json:"data,omitempty"`
// }

// // PushResponse is the JSON stucture for the push response.
// type PushResponse struct {
// 	Request    *http.Request
// 	Writer     http.ResponseWriter
// 	StatusCode int
// 	Error      *APIError
// 	Body       PushResponseBody
// }

// // GetBody returns the Body property.
// func (response *PushResponse) GetBody() interface{} {
// 	return response.Body
// }
//
// // GetCode returns the StatusCode property.
// func (response *PushResponse) GetCode() int {
// 	return response.StatusCode
// }
//
// // SetCode set the StatusCode property.
// func (response *PushResponse) SetCode(statusCode int) {
// 	response.StatusCode = statusCode
// }
//
// // GetError returns the Error property.
// func (response *PushResponse) GetError() *APIError {
// 	return response.Error
// }
//
// // GetWriter returns the Writer property.
// func (response *PushResponse) GetWriter() http.ResponseWriter {
// 	return response.Writer
// }
//
// // SetWriter sets the Writer property.
// func (response *PushResponse) SetWriter(w http.ResponseWriter) {
// 	response.Writer = w
// }
//
// // GetBodyErrors returns response.Body.Errors.
// func (response *PushResponse) GetBodyErrors() []*APIError {
// 	return response.Body.Errors
// }
//
// // SetBodyErrors sets the Errors sub-property of the Body property.
// func (response *PushResponse) SetBodyErrors(apiErrors []*APIError) {
// 	response.Body.Errors = apiErrors
// }
//
// // MapLinks maps the links into the JSON response structure for the push response.
// func (response *PushResponse) MapLinks() {
// 	response.Body.Links.Self = &Self{
// 		Href: response.Request.URL.Path,
// 		Meta: &Meta{
// 			Method: "POST",
// 		},
// 	}
// }
//
// // Send maps the errors, links and data; writes the headers; encodes the JSON, and sends the response.
// func (response *PushResponse) Send(statusCode int) {
// 	if http.StatusText(statusCode) != "" && statusCode != 0 {
// 		response.StatusCode = statusCode
// 	}
//
// 	// MapErrors(response)
// 	response.MapLinks()
// 	SetHeaders(response)
// 	Encode(response)
// }

// /*----------------------------------------------------------------------------
// Handlers
// ----------------------------------------------------------------------------*/

// PushLinks are the base links for the push response.
type PushLinks struct {
}

// Map populates the struct.
func (pl *PushLinks) Map(response *Response) {

}

// PushHandler pushes a notification via Apple's APNS service.
func PushHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	var (
		err   error
		ppReq PushPOSTRequest
	)

	response := NewResponse()
	response.Request = r
	response.Writer = w
	response.Links = &PushLinks{}

	// Decode the POSTed JSON.
	decoder := json.NewDecoder(r.Body)
	if err = decoder.Decode(&ppReq); err != nil {
		response.AddError(0, "Could not decode push POST request JSON", err)
		response.Send(http.StatusBadRequest)
		return
	}

	// Get the service.
	if _, ok := Services[ppReq.Data.Attributes.ApplicationID]; !ok {
		if err = InitializeService(ppReq.Data.Attributes.ApplicationID); err != nil {
			if err == ErrAppNotFound {
				// Return a 404.
				response.AddError(0, "Not found", err)
				response.Send(http.StatusNotFound)
				return
			}

			response.AddError(0, "Could not intialize the service", err)
			response.Send(http.StatusInternalServerError)
			return
		}
	}

	// Assemble the payload.
	p := payload.APS{
		Alert: payload.Alert{Body: ppReq.Data.Attributes.Message},
		Badge: badge.New(ppReq.Data.Attributes.Badge),
	}

	// Send the push.
	service := Services[ppReq.Data.Attributes.ApplicationID]
	apnsID, err := service.Push(ppReq.Data.Attributes.DeviceToken, &push.Headers{}, p)
	if err != nil {
		// TODO: Make the error message and status specific to the error from Apple.
		response.AddError(0, "Could not push the message", err)
		response.Send(http.StatusInternalServerError)
		return
	}
	fmt.Println(apnsID)
}
