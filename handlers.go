package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/jackc/pgx"
	"goji.io/pat"
	"golang.org/x/net/context"
)

// Objecter defines the behaviors of the object.
type Objecter interface {
	GetTable() string
	GetTypeName() string
	GetID() int32
	SetID(int32)
	GetCreationDatetime() time.Time
	SetCreationDatetime(time.Time)
	GetUpdateDatetime() time.Time
	SetUpdateDatetime(time.Time)
	KVPairs() map[string]interface{}
	Scan(scanner) error
	MapOutput() interface{}
}

// ListHandler returns a list of objects from the db.
func ListHandler(ctx context.Context, response *Response) {
	var (
		err    error
		detail = []Detail{}
	)

	// Get the query params.
	params := NewListQueryParams()
	// if err = gestalt.ProcessParams(r.URL.Query(), &params); err != nil {
	// 	if err == gestalt.ErrInvalidQueryParam {
	// 		response.AddError(0, "Invalid list query params", err)
	// 		response.Send(http.StatusInternalServerError)
	// 		return
	// 	}
	//
	// 	response.AddError(0, "Could not process the list query params", err)
	// 	response.Send(http.StatusInternalServerError)
	// 	return
	// }

	// Count the objects in the db.
	params.Count, err = Clients.Postgres.RowCount(response.Object, "")
	if err != nil {
		if err == pgx.ErrNoRows {
			// Return a 404.
			response.AddError(0, "Not found", errors.New("No objects found in the db"))
			response.Send(http.StatusNotFound)
			return
		}

		response.AddError(0, "Unable to count the objects in the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Get the objects from the db.
	rows, err := Clients.Postgres.Query(fmt.Sprintf(string(commonQueries["list"]), response.Object.GetTypeName()))
	if err != nil {
		response.AddError(0, "Unable to get the objects from the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}
	defer rows.Close()

	// Interate through the returned objects.
	for rows.Next() {
		if err = response.Object.Scan(rows); err != nil {
			response.AddError(0, "Unable to scan the object db row", err)
			response.Send(http.StatusInternalServerError)
			return
		}

		detail = append(detail, Detail{
			Type:       response.Object.GetTypeName(),
			ID:         strconv.Itoa(int(response.Object.GetID())),
			Attributes: response.Object.MapOutput(),
		})
	}

	// Respond
	response.Content = detail
	response.Send(0)
}

// MapToer can map data to an interface{}.
type MapToer interface {
	MapTo(interface{}) error
}

// CreateHandler creates resources.
func CreateHandler(ctx context.Context, response *Response, postBody MapToer) {
	var err error

	// Decode the POSTed JSON.
	decoder := json.NewDecoder(response.Request.Body)
	if err = decoder.Decode(&postBody); err != nil {
		response.AddError(0, "Could not decode object POST request JSON", err)
		response.Send(http.StatusBadRequest)
		return
	}

	/*----------------------------------------------------------------------------
	Check the db for an existing resource.
	----------------------------------------------------------------------------*/
	// response.Object.SetName(postBody.Data.Attributes.Name)

	exists, err := Clients.Postgres.RowExists(response.Object)
	if err != nil {
		response.AddError(0, "Unable to check existence of object db record", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	if !exists {
		/*----------------------------------------------------------------------------
		Create a new resource.
		----------------------------------------------------------------------------*/
		// Map the fields in the request to the fields in the object struct.
		if err = postBody.MapTo(response.Object); err != nil {
			response.AddError(0, "Unable to map fields in the request JSON to fields in the object struct", err)
			response.Send(http.StatusInternalServerError)
			return
		}

		// Save the object to the db.
		var ID int32
		row := Clients.Postgres.RowCreate(response.Object)
		if err = row.Scan(&ID); err != nil {
			response.AddError(int(ID), "Unable to insert the object into the db", err)
			response.Send(http.StatusInternalServerError)
			return
		}
		response.Object.SetID(ID)

		if response.Object.GetID() > 0 {
			// Return the response indicating the creation of the object.
			response.Content = Detail{
				Type:       response.Object.GetTypeName(),
				ID:         strconv.Itoa(int(response.Object.GetID())),
				Attributes: response.Object.MapOutput(),
			}
			response.Send(http.StatusCreated)
		}
	} else {
		// Return a 409 (Conflict).
		response.AddError(0, "Object already exists", nil)
		response.Send(http.StatusConflict)
		return
	}
}

// DetailHandler delivers the details for a single resource.
func DetailHandler(ctx context.Context, response *Response) {
	var err error

	// Get the object id from the URL.
	id, err := strconv.Atoi(pat.Param(ctx, "id"))
	if err != nil {
		response.AddError(id, "Unable to convert the object `id` param. to an int", err)
		response.Send(http.StatusBadRequest)
		return
	}
	response.Object.SetID(int32(id))

	// Get the object from the db.
	row := Clients.Postgres.RowRead(response.Object)
	if err := response.Object.Scan(row); err != nil {
		if err == pgx.ErrNoRows {
			// Return a 404.
			response.AddError(int(response.Object.GetID()), "Not found",
				errors.New("The object is not found in the db"))
			response.Send(http.StatusNotFound)
			return
		}

		response.AddError(id, "Unable to retrieve the object from the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Return the response containing the object's detail.
	response.Content = Detail{
		Type:       response.Object.GetTypeName(),
		ID:         strconv.Itoa(int(response.Object.GetID())),
		Attributes: response.Object.MapOutput(),
	}
	response.Send(0)
}

// DeleteHandler deletes an existing resource.
func DeleteHandler(ctx context.Context, response *Response) {
	// Get the object id from the URL.
	id, err := strconv.Atoi(pat.Param(ctx, "id"))
	if err != nil {
		response.AddError(id, "Unable to convert the object `id` param to an int", err)
		response.Send(http.StatusInternalServerError)
		return
	}
	response.Object.SetID(int32(id))

	// Delete the object from the db.
	_, err = Clients.Postgres.RowDelete(response.Object)
	if err != nil {
		response.AddError(id, "Unable to delete the object from the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Return the response indicating the deletion of the object.
	response.Send(0)
}

// UpdateHandler updates an existing object resource.
func UpdateHandler(ctx context.Context, response *Response, postBody MapToer) {
	var err error

	// Get the object id from the URL.
	id, err := strconv.Atoi(pat.Param(ctx, "id"))
	if err != nil {
		response.AddError(id, "Unable to convert the id param to an int", err)
		response.Send(http.StatusInternalServerError)
		return
	}
	response.Object.SetID(int32(id))

	// Decode the incoming JSON.
	decoder := json.NewDecoder(response.Request.Body)
	if err = decoder.Decode(&postBody); err != nil {
		response.AddError(0, "Could not decode PATCH request JSON", err)
		response.Send(http.StatusBadRequest)
		return
	}

	// Get the object from the db.
	row := Clients.Postgres.RowRead(response.Object)
	if err := response.Object.Scan(row); err != nil {
		if err == pgx.ErrNoRows {
			// Return a 404.
			response.AddError(int(response.Object.GetID()), "Not found",
				errors.New("The object is not found in the db"))
			response.Send(http.StatusNotFound)
			return
		}

		response.AddError(id, "Unable to retrieve the object from the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Map the fields in the request body to the object object.
	if err = postBody.MapTo(response.Object); err != nil {
		response.AddError(id, "Unable to map the PATCH data to the object object", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Update the object in the db.
	_, err = Clients.Postgres.RowUpdate(response.Object)
	if err != nil {
		response.AddError(id, "Unable to update the object in the db", err)
		response.Send(http.StatusInternalServerError)
		return
	}

	// Return the response containing the object's detail.
	response.Content = Detail{
		Type:       response.Object.GetTypeName(),
		ID:         strconv.Itoa(int(response.Object.GetID())),
		Attributes: response.Object.MapOutput(),
	}
	response.Send(0)
}

// Get the timeout handler ready to drop some slow requests.
func timeoutHandler(h http.Handler) http.Handler {
	return http.TimeoutHandler(h, time.Duration(Config.HandlerTimeout)*time.Second, "timed out")
}
