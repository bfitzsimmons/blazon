package main

import (
	"errors"
	"fmt"
)

// Errors
var (
	ErrNoID = errors.New("Unable to get an object ID")
)

/*----------------------------------------------------------------------------
Links
----------------------------------------------------------------------------*/

// LinksMapper is the interface for mapping links into an API response.
type LinksMapper interface {
	Map(*Response)
}

// LinkMeta is the "meta" sub-property contained within the "links" JSON substructure present in all API responses.
type LinkMeta struct {
	Method string `json:"method,omitempty"`
}

// Self is the "self" property of the "links" JSON substructure present in all API responses.
type Self struct {
	Href string    `json:"href"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// Find is the "find" property of the "links" JSON substructure present in all API responses.
type Find struct {
	Href string    `json:"href"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// Create is the "create" property of the "links" JSON substructure present in all API responses.
type Create struct {
	Href string    `json:"href"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// Read is the "read" property of the "links" JSON substructure present in all API responses.
type Read struct {
	Href string    `json:"href,omitempty"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// Update is the "update" property of the "links" JSON substructure present in all API responses.
type Update struct {
	Href string    `json:"href,omitempty"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// Delete is the "delete" property of the "links" JSON substructure present in all API responses.
type Delete struct {
	Href string    `json:"href,omitempty"`
	Meta *LinkMeta `json:"meta,omitempty"`
}

// ListLinks are the base links for the list response.
type ListLinks struct {
	Self   *Self   `json:"self"`
	Create *Create `json:"create"`
	PaginationLinks
}

// Map populates the struct.
func (ll *ListLinks) Map(response *Response) {
	ll.Self = &Self{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "GET",
		},
	}

	ll.Create = &Create{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "POST",
		},
	}

	ll.PaginationLinks.Map(response)
}

// IDer is the interface for getting an object's ID.
type IDer interface {
	GetID() int32
}

// CreateLinks are the base links for the create response.
type CreateLinks struct {
	Self   *Self   `json:"self"`
	Find   *Find   `json:"find"`
	Create *Create `json:"create"`
	Read   *Read   `json:"read,omitempty"`
	Update *Update `json:"update,omitempty"`
	Delete *Delete `json:"delete,omitempty"`
}

// Map populates the struct.
func (cl *CreateLinks) Map(response *Response, isErrors bool) error {
	cl.Self = &Self{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "POST",
		},
	}

	cl.Find = &Find{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "GET",
		},
	}

	cl.Create = &Create{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "POST",
		},
	}

	if !isErrors {
		var objPath string
		obj, ok := response.Content.(IDer)
		if !ok {
			return ErrNoID
		}

		objPath = fmt.Sprintf("%s%d", response.Request.URL.Path, obj.GetID())

		cl.Read = &Read{
			Href: objPath,
			Meta: &LinkMeta{
				Method: "GET",
			},
		}

		cl.Update = &Update{
			Href: objPath,
			Meta: &LinkMeta{
				Method: "PATCH",
			},
		}

		cl.Delete = &Delete{
			Href: objPath,
			Meta: &LinkMeta{
				Method: "DELETE",
			},
		}
	}

	return nil
}

// DetailLinks are the base links for the detail response.
type DetailLinks struct {
	Self   *Self   `json:"self"`
	Find   *Find   `json:"find"`
	Create *Create `json:"create"`
	Update *Update `json:"update,omitempty"`
	Delete *Delete `json:"delete,omitempty"`
}

// Map populates the struct.
func (dl *DetailLinks) Map(response *Response, isErrors bool) {
	path, _ := GetParentPath(response.Request.URL.Path)

	dl.Self = &Self{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "GET",
		},
	}

	dl.Find = &Find{
		Href: path,
		Meta: &LinkMeta{
			Method: "GET",
		},
	}

	dl.Create = &Create{
		Href: path,
		Meta: &LinkMeta{
			Method: "POST",
		},
	}

	if !isErrors {
		dl.Update = &Update{
			Href: response.Request.URL.Path,
			Meta: &LinkMeta{
				Method: "PATCH",
			},
		}

		dl.Delete = &Delete{
			Href: response.Request.URL.Path,
			Meta: &LinkMeta{
				Method: "DELETE",
			},
		}
	}
}

// UpdateLinks are the base links for the update response.
type UpdateLinks struct {
	Self   *Self   `json:"self"`
	Find   *Find   `json:"find"`
	Create *Create `json:"create,omitempty"`
	Read   *Read   `json:"read,omitempty"`
	Delete *Delete `json:"delete,omitempty"`
}

// Map populates the struct.
func (ul *UpdateLinks) Map(response *Response, isErrors bool) {
	ul.Self = &Self{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "PATCH",
		},
	}

	path, _ := GetParentPath(response.Request.URL.Path)

	ul.Find = &Find{
		Href: path,
		Meta: &LinkMeta{
			Method: "GET",
		},
	}

	if !isErrors {
		ul.Create = &Create{
			Href: path,
			Meta: &LinkMeta{
				Method: "POST",
			},
		}

		ul.Read = &Read{
			Href: response.Request.URL.Path,
			Meta: &LinkMeta{
				Method: "GET",
			},
		}

		ul.Delete = &Delete{
			Href: response.Request.URL.Path,
			Meta: &LinkMeta{
				Method: "DELETE",
			},
		}
	}
}

// DeleteLinks are the base links for the delete response.
type DeleteLinks struct {
	Self   *Self   `json:"self"`
	Find   *Find   `json:"find,omitempty"`
	Create *Create `json:"create,omitempty"`
}

// Map populates the struct.
func (dl *DeleteLinks) Map(response *Response, isErrors bool) {
	dl.Self = &Self{
		Href: response.Request.URL.Path,
		Meta: &LinkMeta{
			Method: "DELETE",
		},
	}

	if !isErrors {
		path, _ := GetParentPath(response.Request.URL.Path)

		dl.Find = &Find{
			Href: path,
			Meta: &LinkMeta{
				Method: "GET",
			},
		}

		dl.Create = &Create{
			Href: path,
			Meta: &LinkMeta{
				Method: "POST",
			},
		}
	}
}

// PaginationLinks contains the info. related to the pagination of an object's list response.
type PaginationLinks struct {
	First    string `json:"first"`
	Previous string `json:"prev,omitempty"`
	Next     string `json:"next,omitempty"`
	Last     string `json:"last"`
}

// Map maps the data in the response to the struct.
func (pl *PaginationLinks) Map(response *Response) {
	pl.First = ""
	pl.Previous = ""
	pl.Next = ""
	pl.Last = ""
}

// BuildPrevious returns a previous page URL for use in the article list response JSON.
func (pl *PaginationLinks) BuildPrevious() {
	// if params.Offset > 0 {
	// Get the new offset.
	// newOffset := gadgets.CalculateOffset(params.Offset, params.Limit, pl.TotalCount, "previous")
	// params.Set("offset", strconv.Itoa(int(newOffset)))
	//
	// if len(params.Encode()) > 0 {
	// 	uri = fmt.Sprintf("%s?%s", r.URL.Path, params.Encode())
	// } else {
	// 	uri = r.URL.Path
	// }
	// }
}

// BuildNext generates the "previous" URL for the API list response.
func (pl *PaginationLinks) BuildNext() {
	// 	// Get the query params from the request.
	// 	params := gadgets.GetQueries(r)
	//
	// 	// Get the offset param.
	// 	offset := gadgets.GetOffset(params)
	//
	// 	// Get the limit param.
	// 	limit := gadgets.GetLimit(params)
	//
	// 	if (offset + limit) < pl.TotalCount {
	// 		// Determine whether or not the offset needs to change.
	// 		newOffset := gadgets.CalculateOffset(offset, limit, pl.TotalCount, "next")
	// 		params.Set("offset", strconv.Itoa(int(newOffset)))
	//
	// 		if len(params.Encode()) > 0 {
	// 			uri = fmt.Sprintf("%s?%s", r.URL.Path, params.Encode())
	// 		} else {
	// 			uri = r.URL.Path
	// 		}
	// 	}
	//
	// 	return uri
}
