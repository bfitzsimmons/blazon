package main

import (
	"fmt"
	"net/http"
	"runtime"

	log "github.com/Sirupsen/logrus"
	"github.com/davecgh/go-spew/spew"
	"github.com/facebookgo/grace/gracehttp"

	"bitbucket.org/bfitzsimmons/chronicle"
)

// SCS is the spew config. state for the spew degugging related package.
var SCS *spew.ConfigState

func init() {
	var err error

	// Set up debugging tools.
	// SCS = &spew.ConfigState{Indent: "\t", ContinueOnMethod: true}
	SCS = &spew.ConfigState{Indent: "\t"}

	// Set up the configs.
	if err = InitializeConfigs(); err != nil {
		log.Fatal(err)
	}

	// Set up logging.
	if err = chronicle.InitializeLogging(Config.LoggingLevel); err != nil {
		log.Fatal(err)
	}
}

func main() {
	var err error

	log.Info("Server starting on ", Config.Port)
	log.Infof("Running %d CPU cores", runtime.NumCPU())

	/*----------------------------------------------------------------------------
	Client initialization.
	----------------------------------------------------------------------------*/
	Clients = &ClientsContainer{}

	// Initialize Postgres.
	Clients.Postgres, err = InitializePostgres(&PostgresConfig{
		PostgresHost:           Config.PostgresHost,
		PostgresPort:           Config.PostgresPort,
		PostgresUser:           Config.PostgresUser,
		PostgresPassword:       Config.PostgresPassword,
		PostgresDatabase:       Config.PostgresDatabase,
		PostgresMaxConnections: Config.PostgresMaxConnections,
	})
	if err != nil {
		log.Fatalf("Unable to connect to Postgres. Is it running? --> %s", err)
	}
	defer Clients.Postgres.Close()
	log.Infof("Postgres client initialized with %d connections", Clients.Postgres.Stat().MaxConnections)

	/*----------------------------------------------------------------------------
	Routers and middleware.
	----------------------------------------------------------------------------*/
	// Set up the router.
	r := Router()
	r.UseC(chronicle.LoggingMiddleware)
	r.Use(timeoutHandler)

	// Fire up the server on the `port` passed from the command line.
	log.Fatal(gracehttp.Serve(
		&http.Server{
			Addr:    fmt.Sprintf(":%d", Config.Port),
			Handler: r,
		},
	))
}
