package main

// ListQueryParams contains the query params. specific to requests for object lists.
type ListQueryParams struct {
	PaginationQueryParams
}

// NewListQueryParams sets the default properties of the ListQueryParams struct, and returns an instance.
func NewListQueryParams() *ListQueryParams {
	return &ListQueryParams{
		PaginationQueryParams{
			Offset: 0,
			Limit:  10,
		},
	}
}
