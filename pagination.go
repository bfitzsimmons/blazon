package main

// PaginationMapper contains the behavior for mapping data related to the PaginationLinks struct.
type PaginationMapper interface {
	Map()
}

// PaginationQueryParams contains the query params. specific to the pagination of object lists.
type PaginationQueryParams struct {
	Offset int `gestalt:"offset,omitempty"`
	Limit  int `gestalt:"limit,omitempty"`
	Count  int64
}

// // NewPaginationQueryParams sets the default properties of the PaginationQueryParams struct, and returns an instance.
// func NewPaginationQueryParams() PaginationQueryParams {
// 	return PaginationQueryParams{
// 		Offset: 0,
// 		Limit:  10,
// 	}
// }
