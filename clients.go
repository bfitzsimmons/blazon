package main

import "errors"

// Errors
var (
	ErrMissingClientsContext = errors.New("Clients missing from context")
)

// Clients contains the client connections to other services.
var Clients *ClientsContainer

// ClientsContainer stores client connections to external services (memcached, s3, etc.).
type ClientsContainer struct {
	Postgres *Postgres
}
