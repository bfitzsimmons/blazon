package main

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"log"
	"os"
)

func main() {
	var err error

	// Open the file.
	file, err := os.Open("../SantaTracker_apns_sandbox.p12")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	// Read the file.
	data, err := ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}

	fmt.Println(data)
	fmt.Println()

	// Print out the base64 encoded string.
	fmt.Println(base64.StdEncoding.EncodeToString(data))
}
