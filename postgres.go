package main

import (
	"fmt"
	"strings"

	"github.com/jackc/pgx"
	"github.com/nleof/goyesql"
)

var commonQueries goyesql.Queries

func init() {
	commonQueries = goyesql.MustParseFile("sql/common.sql")
}

// RowReader is the interface for reading rows from the db.
type RowReader interface {
	GetTable() string
	GetID() int32
}

// RowWriter is the interface for writing rows to the db.
type RowWriter interface {
	RowReader
	KVPairs() map[string]interface{}
	CreationDatetimer
}

// PostgresConfig holds the config. data for the Postgres connection.
type PostgresConfig struct {
	PostgresHost           string
	PostgresPort           uint16
	PostgresUser           string
	PostgresPassword       string
	PostgresDatabase       string
	PostgresMaxConnections int
}

// Postgres wraps the Postgres client so that functionality may be added if needed.
type Postgres struct {
	*pgx.ConnPool
}

// InitializePostgres initializes the connections to the Postgres host.
func InitializePostgres(pgConfig *PostgresConfig) (*Postgres, error) {
	// Configure the connection.
	config := pgx.ConnConfig{
		Host:     pgConfig.PostgresHost,
		Port:     pgConfig.PostgresPort,
		User:     pgConfig.PostgresUser,
		Password: pgConfig.PostgresPassword,
		Database: pgConfig.PostgresDatabase,
	}

	// Configure the connection pool.
	poolConfig := pgx.ConnPoolConfig{
		ConnConfig:     config,
		MaxConnections: pgConfig.PostgresMaxConnections,
	}

	// Create the connection pool.
	connectionPool, err := pgx.NewConnPool(poolConfig)
	if err != nil {
		return nil, err
	}

	return &Postgres{connectionPool}, nil
}

// RowCount returns a count of the records in the db.
func (pg *Postgres) RowCount(rr RowReader, where string) (int64, error) {
	if where == "" {
		where = "true"
	}

	var count int64
	row := pg.QueryRow(fmt.Sprintf(commonQueries["count"], rr.GetTable(), where))
	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

// RowRead retrieves a single object row from the db.
func (pg *Postgres) RowRead(rr RowReader) *pgx.Row {
	return pg.QueryRow(
		fmt.Sprintf(commonQueries["read"], rr.GetTable()),
		rr.GetID(),
	)
}

// RowCreate inserts the object into the db.
func (pg *Postgres) RowCreate(rw RowWriter) *pgx.Row {
	// Get the field names and values.
	pairs := rw.KVPairs()
	fields := make([]string, len(pairs))
	values := make([]interface{}, len(pairs))
	i := 0
	for k, v := range pairs {
		fields[i] = k
		values[i] = v
		i++
	}

	fieldNames := strings.Join(fields, ", ")

	// Get the placeholders.
	placeholders := ""
	for i := range values {
		placeholders += fmt.Sprintf("$%d, ", i+1)
	}
	placeholders = strings.TrimSuffix(placeholders, ", ")

	// Run the query.
	return pg.QueryRow(
		fmt.Sprintf(commonQueries["create"], rw.GetTable(), fieldNames, placeholders),
		values...,
	)
}

// RowUpdate updates the object in the db.
func (pg *Postgres) RowUpdate(rw RowWriter) (pgx.CommandTag, error) {
	// Get the field names and values.
	pairs := rw.KVPairs()
	fields := make([]string, len(pairs))
	values := make([]interface{}, len(pairs))
	i := 0
	for k, v := range pairs {
		fields[i] = k
		values[i] = v
		i++
	}

	data := ""
	i = 0
	for i = range values {
		data += fmt.Sprintf("%s=$%d, ", fields[i], i+1)
	}
	data = strings.TrimSuffix(data, ", ")

	// Add the ID to the values.
	values = append(values, rw.GetID())

	// Run the query.
	return pg.Exec(
		fmt.Sprintf(commonQueries["update"], rw.GetTable(), data, fmt.Sprintf("$%d", i+2)),
		values...,
	)
}

// RowDelete deletes the object from the db.
func (pg *Postgres) RowDelete(rr RowReader) (pgx.CommandTag, error) {
	// Run the query.
	return pg.Exec(
		fmt.Sprintf(commonQueries["delete"], rr.GetTable()),
		rr.GetID(),
	)
}

// RowExists tests for the existence of the object in the db.
func (pg *Postgres) RowExists(rr RowReader) (bool, error) {
	var (
		err    error
		exists bool
	)

	// Run the query.
	err = pg.QueryRow(
		fmt.Sprintf(commonQueries["exists"], rr.GetTable()),
		rr.GetID(),
	).Scan(&exists)
	return exists, err
}

// type Where struct {
// 	Operation  string
// 	FieldName  string
// 	Operator   string
// 	FieldValue interface{}
// }
