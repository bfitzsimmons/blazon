package main

// APIError contains info. about an error occuring during the processing of an API request/response.
type APIError struct {
	ID     int    `json:"id,omitempty"`
	Title  string `json:"title,omitempty"`
	Detail string `json:"detail,omitempty"`
}
