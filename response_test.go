package main

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewResponse(t *testing.T) {
	// Set up the response.
	response := NewResponse()

	// Make the response type is correct.
	assert.IsType(t, response, &Response{})

	// Make sure the status code is set to 200.
	if response.StatusCode != 200 {
		t.Errorf("Expected 200, got %d", response.StatusCode)
	}
}

func TestMeta_Map(t *testing.T) {
	meta := Meta{}
	meta.Map()

	// How did we do?
	if meta.Version != Config.Version {
		t.Errorf("Expected %s, got %s", Config.Version, meta.Version)
	}
}

func TestResponseBody_MapErrors(t *testing.T) {
	testData := []struct {
		statusCode  int
		err         *APIError
		responseErr []*APIError
		mapErrorErr error
	}{
		// No errors.
		{200, nil, nil, nil},

		// Non-5xx errors.
		{400, &APIError{}, []*APIError{&APIError{}}, nil},

		// 5xx errors.
		{500, &APIError{}, []*APIError{&APIError{}}, nil},

		// Invalid status code.
		{8993, &APIError{}, []*APIError{&APIError{}}, ErrInvalidStatusCode},
	}

	for _, data := range testData {
		// Set up the response.
		response := NewResponse()
		response.StatusCode = data.statusCode
		response.Error = data.err

		// Set up the response body.
		responseBody := ResponseBody{}

		// Map the errors.
		err := responseBody.MapErrors(response)

		// How did we do?
		if !reflect.DeepEqual(err, data.mapErrorErr) {
			t.Errorf("%d -- Expected %v, got %v", data.statusCode, data.mapErrorErr, err)
		}

		if !reflect.DeepEqual(responseBody.Errors, data.responseErr) {
			t.Errorf("Expected %v, got %v", data.responseErr, responseBody.Errors)
		}
	}
}

func TestResponse_AddError(t *testing.T) {
	testData := []struct {
		id          int
		title       string
		err         error
		responseErr *APIError
	}{
		{
			0,
			"It's an error",
			errors.New("Yup. It's an error"),
			&APIError{0, "It's an error", "Yup. It's an error"},
		},
		{
			45,
			"It's another error",
			errors.New("Yup. It's another error"),
			&APIError{45, "It's another error", "Yup. It's another error"},
		},
		{
			0,
			"",
			errors.New(""),
			&APIError{0, "", ""},
		},
	}

	for _, data := range testData {
		// Set up the response.
		response := NewResponse()

		// Add the error to the response.
		response.AddError(data.id, data.title, data.err)

		// How did we do?
		if !reflect.DeepEqual(response.Error, data.responseErr) {
			t.Errorf("Expected %v, got %v", data.responseErr, response.Error)
		}
	}
}

func TestResponse_IsErrors(t *testing.T) {
	response := NewResponse()

	// No errors.
	if response.IsErrors() {
		t.Errorf("Expected false, got %t", response.IsErrors())
	}

	// Errors
	response.Error = &APIError{}
	if !response.IsErrors() {
		t.Errorf("Expected true, got %t", response.IsErrors())
	}
}

func TestResponse_AddContent(t *testing.T) {
	var err error

	// Set up the app.
	app := Application{}

	// Set up the response.
	response := NewResponse()

	// Add the app to the response.
	err = response.AddContent(app)

	// How did we do?
	assert.Nil(t, err)

	// TODO: test the contents of response.Content.
}

func TestResponse_SetHeaders(t *testing.T) {
	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w
	response.Object = &Application{}
	response.StatusCode = 201

	// Set the headers.
	err = response.SetHeaders()

	// How did we do?
	assert.Nil(t, err)

	if response.Writer.Header().Get("Content-Type") != "application/vnd.api+json" {
		t.Errorf("Expected application/vnd.api+json, got %s", response.Writer.Header().Get("Content-Type"))
	}

	if response.Writer.Header().Get("Cache-Control") != "no-cache" {
		t.Errorf("Expected no-cache, got %s", response.Writer.Header().Get("Cache-Control"))
	}

	if response.Writer.Header().Get("Location") != "/api/apps/0" {
		t.Errorf("Expected /api/apps/0, got %s", response.Writer.Header().Get("Location"))
	}

	// With invalid status code.
	response.StatusCode = 8000
	err = response.SetHeaders()
	assert.NotNil(t, err)
}

func TestResponse_MapLinks(t *testing.T) {
	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w
	response.Object = &Application{}
	response.Links = &ApplicationDetailLinks{}

	// Set up the response body.
	responseBody := ResponseBody{}

	// Map the links.
	response.MapLinks(&responseBody)

	// How did we do?
	if !reflect.DeepEqual(response.Links, responseBody.Links) {
		t.Errorf("Expected %v, for %v", response.Links, responseBody.Links)
	}
}

func TestResponse_MapMeta(t *testing.T) {
	// Set up the response and response body.
	response := NewResponse()
	responseBody := ResponseBody{}

	// Map the meta.
	response.MapMeta(&responseBody)

	// How did we do?
	if !reflect.DeepEqual(response.Meta, responseBody.Meta) {
		t.Errorf("Expected %v, got %v", responseBody.Meta, response.Meta)
	}
}

func TestResponse_Encode(t *testing.T) {
	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w
	response.Object = &Application{}
	response.Links = &ApplicationDetailLinks{}

	// Set up the response body.
	responseBody := ResponseBody{}

	// Encode the response.
	err = response.Encode(&responseBody)

	// How did we do?
	assert.Nil(t, err)
}

func TestResponse_Send(t *testing.T) {
	// Set up the request.
	req, err := http.NewRequest("GET", "/api/apps/", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Set up the handler.
	w := httptest.NewRecorder()

	// Set up the response.
	response := NewResponse()
	response.Request = req
	response.Writer = w
	response.Object = &Application{}
	response.Links = &ApplicationDetailLinks{}

	// Send the response with no status code.
	response.Send(0)

	// Send the response with a status code.
	response.Send(201)

	// Send the response with an invalid status code.
	response.Send(8000)
}
