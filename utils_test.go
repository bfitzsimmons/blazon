package main

import (
	"net/url"
	"reflect"
	"testing"
	"time"
)

func TestSetCreationDatetime(t *testing.T) {
	app := &Application{}

	// When the time is already set.
	app.CreationDatetime = time.Now()
	oldTime := app.CreationDatetime
	SetCreationDatetime(app, time.Now())
	if app.CreationDatetime != oldTime {
		t.Errorf("When the time is already set I expected %v, got %v", oldTime, app.CreationDatetime)
	}

	// When the time is not set.
	app.CreationDatetime = time.Time{}
	currentTime := time.Now()
	SetCreationDatetime(app, currentTime)
	if app.CreationDatetime != currentTime.UTC() {
		t.Errorf("When the time is not set I expected %v, got %v", currentTime, app.CreationDatetime)
	}
}

func TestGetParentPath(t *testing.T) {
	testData := []struct {
		url  string
		path string
		err  error
	}{
		{"/api/apps/45", "/api/apps", nil},
		{"/api/apps/45/", "/api/apps", nil},
		{"/api/apps", "/api", nil},
		{"/api/apps/", "/api", nil},
		{"/api", "", nil},
		{"/api/", "", nil},
		{"", "", nil},
		{"$$$://()*&^%$#@! 123@", "", &url.Error{Op: "parse", URL: "$$$://()*&^%$", Err: url.EscapeError("%$")}},
	}

	for _, data := range testData {
		path, err := GetParentPath(data.url)

		if !reflect.DeepEqual(data.err, err) {
			t.Errorf("Expected %v, got %v", data.err, err)
		}

		if path != data.path {
			t.Errorf("Expected %s, got %s", data.path, path)
		}
	}
}
