package main

type contextKey int

const (
	// ClientsKey is the context key used to store the clients used to connect to external services (db, http, etc.).
	ClientsKey contextKey = iota
)
