package main

import (
	"github.com/RobotsAndPencils/buford/certificate"
	"github.com/RobotsAndPencils/buford/push"
	"github.com/jackc/pgx"
)

// Services holds the APNS connections.
var Services map[int32]push.Service

func init() {
	Services = make(map[int32]push.Service)
}

// InitializeService sets up an APNS service.
func InitializeService(ID int32) error {
	app := Application{}
	app.ID = ID

	// Get the Application from the db.
	row := Clients.Postgres.RowRead(&app)
	if err := app.Scan(row); err != nil {
		if err == pgx.ErrNoRows {
			return ErrAppNotFound
		}
		return err
	}

	cert, err := certificate.Decode(app.CertData, app.Password)
	if err != nil {
		return err
	}

	Services[app.ID] = push.Service{
		Client: push.NewClient(cert),
		Host:   push.Development,
	}

	return nil
}
