package main

import (
	"encoding/base64"
	"errors"
	"net/http"
	"time"

	"goji.io/pattern"

	"golang.org/x/net/context"
)

// Errors
var (
	ErrAppNotFound = errors.New("The application was not found")
)

/*----------------------------------------------------------------------------
Models
----------------------------------------------------------------------------*/

// Application contains the Apple application's info.
type Application struct {
	ID               int32     `json:"id"`
	Name             string    `json:"name"`
	CertData         []byte    `json:"cert_data"`
	Password         string    `json:"password"`
	CreationDatetime time.Time `json:"creation_datetime"`
	UpdateDatetime   time.Time `json:"update_datetime"`
}

// GetID returns the Application's ID.
func (app *Application) GetID() int32 {
	return app.ID
}

// SetID sets the ID property.
func (app *Application) SetID(ID int32) {
	app.ID = ID
}

// GetCreationDatetime returns the creation datetime.
func (app *Application) GetCreationDatetime() time.Time {
	return app.CreationDatetime
}

// SetCreationDatetime sets the creation datetime.
func (app *Application) SetCreationDatetime(currentTime time.Time) {
	app.CreationDatetime = currentTime.UTC()
}

// GetUpdateDatetime returns the update datetime.
func (app *Application) GetUpdateDatetime() time.Time {
	return app.UpdateDatetime
}

// SetUpdateDatetime sets the update datetime.
func (app *Application) SetUpdateDatetime(currentTime time.Time) {
	app.UpdateDatetime = currentTime.UTC()
}

// GetTable returns the db table name.
func (app *Application) GetTable() string {
	return "apps"
}

// GetTypeName returns the name for this object type.
func (app *Application) GetTypeName() string {
	return "apps"
}

// KVPairs returns a slice of fields.
func (app *Application) KVPairs() map[string]interface{} {
	return map[string]interface{}{
		"name":              app.Name,
		"cert_data":         app.CertData,
		"password":          app.Password,
		"creation_datetime": app.CreationDatetime,
		"update_datetime":   app.UpdateDatetime,
	}
}

type scanner interface {
	Scan(dest ...interface{}) (err error)
}

// Scan maps the data in a db row to a Application struct.
func (app *Application) Scan(row scanner) error {
	return row.Scan(
		&app.ID,
		&app.Name,
		&app.CertData,
		&app.Password,
		&app.CreationDatetime,
		&app.UpdateDatetime,
	)
}

// MapOutput maps the attrs. in the Application to the attrs. in the ApplicationOutput struct.
func (app *Application) MapOutput() interface{} {
	return struct {
		Name             string    `json:"name"`
		CertData         string    `json:"cert_data"`
		Password         string    `json:"password"`
		CreationDatetime time.Time `json:"creation_datetime"`
		UpdateDatetime   time.Time `json:"update_datetime"`
	}{
		Name:             app.Name,
		CertData:         base64.StdEncoding.EncodeToString(app.CertData),
		Password:         app.Password,
		CreationDatetime: app.CreationDatetime,
		UpdateDatetime:   app.UpdateDatetime,
	}
}

/*----------------------------------------------------------------------------
Links
----------------------------------------------------------------------------*/

// // ApplicationLinksExtra is an extra link.
// type ApplicationLinksExtra struct {
// 	Href string    `json:"href"`
// 	Meta *LinkMeta `json:"meta,omitempty"`
// }
//
// // Map populates the struct.
// func (links *ApplicationLinksExtra) Map() {
// 	links.Href = "http://example.com/extra/"
// 	links.Meta = &LinkMeta{
// 		Method: "GET",
// 	}
// }
//
// // ApplicationLinksExtra2 is another extra link.
// type ApplicationLinksExtra2 struct {
// 	Href string    `json:"href"`
// 	Meta *LinkMeta `json:"meta,omitempty"`
// }
//
// // Map populates the struct.
// func (links *ApplicationLinksExtra2) Map() {
// 	links.Href = "http://example.com/extra2/"
// 	links.Meta = &LinkMeta{
// 		Method: "GET",
// 	}
// }

// ApplicationLinks contains any application specific links that need to be added to any/all Application API responses.
type ApplicationLinks struct {
	// Extra  ApplicationLinksExtra  `json:"extra"`
	// Extra2 ApplicationLinksExtra2 `json:"extra2"`
}

// Map maps the Application specific links.
func (links *ApplicationLinks) Map(response *Response) {
	// links.Extra.Map()
	// links.Extra2.Map()
}

// ApplicationListLinks are the links for the Application list response.
type ApplicationListLinks struct {
	ListLinks
	ApplicationLinks
}

// Map maps the links.
func (links *ApplicationListLinks) Map(response *Response) {
	// Populate the list specific links.
	links.ListLinks.Map(response)

	// Populate the Application list specific links.
	links.ApplicationLinks.Map(response)
}

// ApplicationCreateLinks are the links for the Application create response.
type ApplicationCreateLinks struct {
	CreateLinks
	ApplicationLinks
}

// Map maps the links.
func (links *ApplicationCreateLinks) Map(response *Response) {
	// Populate the list specific links.
	links.CreateLinks.Map(response, response.IsErrors())

	// Populate the Application list specific links.
	links.ApplicationLinks.Map(response)
}

// ApplicationDetailLinks are the links for the Application detail response.
type ApplicationDetailLinks struct {
	DetailLinks
	ApplicationLinks
}

// Map maps the links.
func (links *ApplicationDetailLinks) Map(response *Response) {
	// Populate the detail specific links.
	links.DetailLinks.Map(response, response.IsErrors())

	// Populate the Application detail specific links.
	links.ApplicationLinks.Map(response)
}

// ApplicationUpdateLinks are the links for the Application update response.
type ApplicationUpdateLinks struct {
	UpdateLinks
	ApplicationLinks
}

// Map maps the links.
func (links *ApplicationUpdateLinks) Map(response *Response) {
	// Populate the delete specific links.
	links.UpdateLinks.Map(response, response.IsErrors())

	// Populate the Application detail specific links.
	links.ApplicationLinks.Map(response)
}

// ApplicationDeleteLinks are the links for the Application delete response.
type ApplicationDeleteLinks struct {
	DeleteLinks
	ApplicationLinks
}

// Map maps the links.
func (links *ApplicationDeleteLinks) Map(response *Response) {
	// Populate the delete specific links.
	links.DeleteLinks.Map(response, response.IsErrors())

	// Populate the Application detail specific links.
	links.ApplicationLinks.Map(response)
}

/*----------------------------------------------------------------------------
Requests
----------------------------------------------------------------------------*/

// ApplicationPOSTRequest is the request body for the Application creation API request.
type ApplicationPOSTRequest struct {
	Data struct {
		Attributes struct {
			Name     string `json:"name"`
			CertData string `json:"cert_data"`
			Password string `json:"password"`
		} `json:"attributes"`
	} `json:"data"`
}

// MapTo maps the fields in the request body to the fields in the Application struct.
func (lpr *ApplicationPOSTRequest) MapTo(obj interface{}) error {
	if app, ok := obj.(*Application); ok {
		certData, err := base64.StdEncoding.DecodeString(lpr.Data.Attributes.CertData)
		if err != nil {
			return err
		}
		app.CertData = certData
		app.Name = lpr.Data.Attributes.Name
		app.Password = lpr.Data.Attributes.Password
		currentTime := time.Now()
		SetCreationDatetime(app, currentTime)
		app.SetUpdateDatetime(currentTime.UTC())
	}

	return nil
}

// ApplicationPATCHRequest is the request body for the Application update API request.
type ApplicationPATCHRequest ApplicationPOSTRequest

// MapTo maps the fields in the request body to the fields in the Application struct.
func (lpr *ApplicationPATCHRequest) MapTo(obj interface{}) error {
	if app, ok := obj.(*Application); ok {
		certData, err := base64.StdEncoding.DecodeString(lpr.Data.Attributes.CertData)
		if err != nil {
			return err
		}
		app.CertData = certData
		app.Name = lpr.Data.Attributes.Name
		app.Password = lpr.Data.Attributes.Password
	}

	return nil
}

/*----------------------------------------------------------------------------
Handlers
----------------------------------------------------------------------------*/

// ApplicationHandler both delivers lists of, and creates, Application objects.
func ApplicationHandler(ctx context.Context, w http.ResponseWriter, r *http.Request) {
	response := NewResponse()
	response.Request = r
	response.Writer = w
	response.Object = &Application{}

	switch r.Method {
	case "GET":
		if _, ok := ctx.Value(pattern.Variable("id")).(string); !ok {
			// List/Find
			response.Links = &ApplicationListLinks{}
			ListHandler(ctx, response)
		} else {
			// Detail
			response.Links = &ApplicationDetailLinks{}
			DetailHandler(ctx, response)
		}
	case "POST":
		// Create
		response.Links = &ApplicationCreateLinks{}
		CreateHandler(ctx, response, &ApplicationPOSTRequest{})
	case "DELETE":
		// Delete
		response.Links = &ApplicationDeleteLinks{}
		DeleteHandler(ctx, response)
	case "PATCH":
		// Update
		response.Links = &ApplicationUpdateLinks{}
		UpdateHandler(ctx, response, &ApplicationPATCHRequest{})
	}
}
