package main

import (
	"goji.io"
	"goji.io/pat"
)

// Router defines the Exposure specific URL pattern routers.
func Router() *goji.Mux {
	mux := goji.NewMux()

	// APIs
	apiSub := goji.SubMux()
	mux.HandleC(pat.New("/api/*"), apiSub)

	/*----------------------------------------------------------------------------
	Application APIs
	----------------------------------------------------------------------------*/
	appSub := goji.SubMux()
	apiSub.HandleC(pat.New("/apps/*"), appSub)

	// List
	appSub.HandleFuncC(pat.Get("/"), ApplicationHandler)

	// Create
	appSub.HandleFuncC(pat.Post("/"), ApplicationHandler)

	// Retrieve, update and delete.
	appURLPattern := "/:id"
	appSub.HandleFuncC(pat.Get(appURLPattern), ApplicationHandler)
	appSub.HandleFuncC(pat.Patch(appURLPattern), ApplicationHandler)
	appSub.HandleFuncC(pat.Delete(appURLPattern), ApplicationHandler)

	/*----------------------------------------------------------------------------
	Push APIs
	----------------------------------------------------------------------------*/
	pushSub := goji.SubMux()
	apiSub.HandleC(pat.New("/push/*"), pushSub)
	pushSub.HandleFuncC(pat.Post("/"), PushHandler)

	/*----------------------------------------------------------------------------
	Misc.
	----------------------------------------------------------------------------*/
	// Favicon.
	// r.HandleFunc("/favicon.ico", FaviconHandler).Methods("GET", "HEAD").Name("favicon")

	return mux
}
