-- Apps
CREATE TABLE "apps" (
	"id" serial NOT NULL PRIMARY KEY,
	"name" varchar(32) NOT NULL UNIQUE,
	"cert_data" bytea NOT NULL,
	"password" varchar(64) NOT NULL,
	"creation_datetime" timestamptz NOT NULL DEFAULT now(),
	"update_datetime" timestamptz NOT NULL DEFAULT now()
);
