package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewListQueryParams(t *testing.T) {
	// Set up the params.
	params := NewListQueryParams()

	// Make sure the response type is correct.
	assert.IsType(t, params, &ListQueryParams{})

	// Make sure the Offset is set to 0.
	if params.Offset != 0 {
		t.Errorf("Expected 0, got %d", params.Offset)
	}

	// Make sure the Limit is set to 10.
	if params.Limit != 10 {
		t.Errorf("Expected 10, got %d", params.Limit)
	}
}
